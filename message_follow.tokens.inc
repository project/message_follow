<?php

/**
 * Implements hook_token_info().
 */
function message_follow_token_info() {
  return [
    'tokens' => [
      'message' => [
        'message-follow-context' => [
          'name' => t('Message follow context'),
          'description' => t('The context a message is being sent from.'),
          'type' => 'node',
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function message_follow_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];

  if ($type === 'message' && ($follow_tokens = token_find_with_prefix($tokens, 'message-follow-context'))) {
    $message_wrapper = entity_metadata_wrapper('message', $data['message']);
    if (isset($message_wrapper->field_target_node)) {
      $node = $message_wrapper->field_target_node->value();
    }
    elseif (isset($message_wrapper->field_target_comment)) {
      $node = $message_wrapper->field_target_comment->node->value();
    }

    if (isset($node) && ($context_node = message_follow_user_context($node, $data['message']->uid))) {
      $replacements += token_generate('node', $follow_tokens, ['node' => $context_node]);
    }
  }

  return $replacements;
}
