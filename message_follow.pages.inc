<?php

/**
 * Form callback for Notifications user page local task.
 */
function message_follow_profile_form($form, $form_state, $account) {
  $content_types = array();
  foreach (message_subscribe_flag_get_flags('node') as $flag) {
    foreach ($flag->types as $content_type) {
      if ($follow_flag = message_follow_get_flag($content_type)) {
        $content_types[$content_type] = $follow_flag;
      }
    }
  }

  $default_value = array();
  $result = db_query('SELECT content_type, message_type, enabled FROM {message_follow_by_type} WHERE uid = :uid', array(':uid' => $account->uid));
  foreach ($result as $row) {
    $key = $row->content_type . '|' . $row->message_type;
    $default_value[$key] = $row->enabled ? $key : FALSE;
  }

  $names = node_type_get_names();
  $options = array();
  foreach ($content_types as $content_type => $flag) {
    if ($message_type = variable_get('message_follow_comment_message_' . $content_type)) {
      $key = $content_type . '|' . $message_type;
      $options[$key] = check_plain($names[$content_type]);
      if (!isset($default_value[$key])) {
        $default_value[$key] = $key;
      }
    }
  }
  $form['comment_notification'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Receive email notifications about comments on content you follow for'),
    '#options' => $options,
    '#default_value' => $default_value,
  );

  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );

  return $form;
}

/**
 * Form submit callback for message_follow_profile_form().
 */
function message_follow_profile_form_submit($form, $form_state) {
  foreach ($form_state['values']['comment_notification'] as $key => $value) {
    list($content_type, $message_type) = explode('|', $key);
    db_merge('message_follow_by_type')
      ->key(array(
        'uid' => $form_state['build_info']['args'][0]->uid,
        'content_type' => $content_type,
        'message_type' => $message_type,
      ))
      ->fields(array(
        'enabled' => (int) (bool) $value,
      ))
      ->execute();
  }
}
