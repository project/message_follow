Message follow is (currently) built for following individual pieces of content
using the Message stack.

After enabling, configuration is needed:
1. Set up a flag to be the following flag at …/admin/structure/flags. The
   Message subscribe module comes with a default flags named subscribe_* which
   can be enabled.
2. For email or other messages, set up a Message type at
   …/admin/structure/messages. A comment notification message will need an
   Entity reference field named field_target_comment.
3. Edit content types to enable following at …/admin/structure/types. Click
   edit, then select the flag on the “Message follow settings” tab.
